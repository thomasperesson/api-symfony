<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Scores;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    private $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 1000; $i++) {
            $score = new Scores();
            $secondes = $faker->numberBetween(0, 999);
            if ($secondes < 10) {
                $secondes = "00" . $secondes;
            } else if($secondes < 100) {
                $secondes = "0" . $secondes;
            }
            $score->setScore($secondes . "." . $faker->numberBetween(0, 99))->setNickname($faker->firstName() . $faker->lastName());
            $manager->persist($score);
        }

        $user = new User();
        $password = $this->hasher->hashPassword($user, "user");
        $user   ->setEmail("user@user.fr")
                ->setPassword($password);
        $manager->persist($user);

        $manager->flush();
    }
}
