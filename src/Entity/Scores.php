<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ScoresRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ScoresRepository::class)
 */
#[ApiResource(
    attributes:[
        "order" => ["score" => "ASC"]
    ]
)]
class Scores
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("score:read")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Groups("score:read")
     * @Assert\NotBlank(message="Le score ne peut pas être vide.")
     */
    private $score;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("score:read")
     * @Assert\NotBlank(message="Le pseudo ne peut pas être vide.")
     * @Assert\Length(
     *      min=3,
     *      max=15,
     *      minMessage="Votre pseudo doit faire au minimum {{ limit }} caractères",
     *      maxMessage="Votre pseudo doit faire au maximum {{ limit }} caractères"
     * )
     */
    private $nickname;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTimeImmutable("now"));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScore(): ?float
    {
        return $this->score;
    }

    public function setScore(float $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
