<?php

namespace App\Controller;

use App\Entity\Scores;
use InvalidArgumentException;
use App\Repository\ScoresRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

#[Route('/api')]
class ApiScoreboardController extends AbstractController
{
    #[Route('/scoreboard', name: 'api_scoreboard', methods: ['GET'])]
    public function sendAllScores(ScoresRepository $scoresRepository): Response
    {
        return $this->json($scoresRepository->findBy([], ['score' => 'ASC']), 200, [], ['groups' => 'score:read']);
    }

    #[Route('/top10', name: 'api_top_10', methods: ['GET'])]
    public function sendTopTenScores(ScoresRepository $scoresRepository): Response
    {
        return $this->json($scoresRepository->getTop(10), 200, [], ['groups' => 'score:read']);
    }
    #[Route('/top1', name: 'api_top_1', methods: ['GET'])]
    public function sendTopOneScore(ScoresRepository $scoresRepository): Response
    {
        return $this->json($scoresRepository->getTop(1), 200, [], ['groups' => 'score:read']);
    }

    #[Route('/scoreboard', name: 'api_scoreboard_create', methods: ['POST'])]
    public function addScore(Request $request, SerializerInterface $serializer, EntityManagerInterface $manager, ValidatorInterface $validator)
    {
        $jsonCatched = $request->getContent();
        $score = $serializer->deserialize($jsonCatched, Scores::class, 'json');

        try {
            $errors = $validator->validate($score);

            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }

            $manager->persist($score);
            $manager->flush();

            return $this->json($score, 201, [], ['groups' => 'score:read']);

        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        } catch (InvalidArgumentException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    #[Route('/scoreboard/{id}', name: 'api_scoreboard_get_one', methods: ['GET'])]
    public function getOneScore(ScoresRepository $scoresRepository, int $id): Response
    {
        return $this->json($scoresRepository->findBy(['id' => $id]), 200, [], ['groups' => 'score:read']);
    }

    #[Route('/scoreboard/{id}', name: 'api_scoreboard_delete', methods: ['DELETE'])]
    public function deleteScore(EntityManagerInterface $manager, ScoresRepository $scoresRepository, int $id): Response
    {
        try {
            $score = $scoresRepository->findOneBy(['id' => $id]);
            $manager->remove($score);
            $manager->flush();
            return $this->json([
                'status' => 200,
                'message' => 'Score supprimé'
            ]);
        } catch(NotFoundHttpException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ]);
        } catch(InvalidArgumentException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ]);
        }
    }
}
